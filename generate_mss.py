#!/usr/bin/env python
import datetime
from datetime import timedelta
import subprocess
import threading
import Queue
import logging
import StringIO
import csv
import netaddr
import sys
import os

hour_ago = str(datetime.datetime.utcnow() - timedelta(hours=1))[:19]
thirty_days_ago = str(datetime.datetime.now() - timedelta(days=30))[:19]
year_ago = str(datetime.datetime.utcnow() - timedelta(days=365))[:19]
hour_ago = hour_ago.replace(' ', 'T')
year_ago = year_ago.replace(' ', 'T')
thirty_days_ago = thirty_days_ago.replace(' ', 'T')
time_stamp = str(datetime.datetime.utcnow())[:13].replace(' ','').replace('-','')
time_stamp = time_stamp + '45'

columns = ['document_source', 'document_name', 'observable_variety',
           'observable_value', 'valid', 'document_received', 'document_tlp',
           'ttp_category', 'ttp_actions', 'description', 'document_tlp',
           'document_nocomm', 'document_noforn']
max_iocdb_queries = 3 #number of IOCDB queries to run at the same time

q = Queue.Queue()

feodob = [] #track these to remove them feodo ipblocklist to find version a

#made-up confidence values for each feed
inputs = {'abuse': '90', 'abuse2': '90', 'atims': '90', 'ciscp': '75',
          'csint': '75', 'govint': '50', 'mss': '80',
          'osint': '60', 'osint30': '60', 'tor': '75', 'vecirt': '90',
          'vzir': '95'}

outputs = {'abuse.ch-zeustracker' : {'confidence' : '90'},
           'abuse.ch-spyeyetracker' : {'confidence' : '90'},
           'abuse.ch-palevotracker' : {'confidence' : '90'},
           'abuse.ch-feodotracker' : {'confidence' : '90'},
           'abuse.ch-feodotracker-a' : {'confidence' : '90'},
           'abuse.ch-feodotracker-b' : {'confidence' : '90'},
           'ATIMS' : {'confidence' : '90'},
           'CISCP' : {'confidence' : '75'},
           'CSINT' : {'confidence' : '75'},
           'GOVINT' : {'confidence' : '50'},
           'MSS' : {'confidence' : '80'},
           'OSINT' : {'confidence' : '60'},
           'OSINT30' : {'confidence' : '60'},
           'TOR-exit' : {'confidence' : '75'},
           'TOR-entry' : {'confidence' : '75'},
           'VECIRT' : {'confidence' : '90'},
           'VZIR' : {'confidence' : '95'}
           }

#delete old files
folder = '/var/data/output/mss'
for the_file in os.listdir(folder):
    file_path = os.path.join(folder, the_file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
    except Exception, e:
        print e

#open files for output
try:
    for output in outputs:
        outputs[output]['file'] = open('/var/data/output/mss/' + \
                                       output + '_' + time_stamp + '.dat', 'w')
        outputs[output]['results'] = []
except StandardError as e:
    logging.error('Error opening files' + '; msg=%s' % e.message)

#query IOCDB for data; will be called for each IOCDB query in parallel
def query_iocdb(q):
    while True: # stay in an infinite loop processing any items in the q
        args = q.get() #thread will block on this until something is in q
        name = args.pop()
        sys.stdout.flush()
        print(str(datetime.datetime.utcnow()) + ' executing ' + name)
        sys.stdout.flush()

        try_number = 0

        try:
            #print(args + columns)
            #sys.stdout.flush()
            p = subprocess.Popen(args + columns,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            out, err = p.communicate()

        except elasticsearch.exceptions.ConnectionError as e:
            logging.error('Error running query: ' + name + \
                          ' Caught elasticsearch.exceptions.ConnectionError' +\
                          'msg=%s' % e.message)
            try_number += 1
            #try again if this is only the second time
            if try_number <= 1:
                logging.error('Trying again.')
                sys.stdout.flush()
                p = subprocess.Popen(args + columns,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                out, err = p.communicate()
            else:
                logging.error('Already tried twice before this; stopping.')
                q.task_done()
                continue #so we don't try to process a query that failed

        except StandardError as e:
            logging.error('Error running query: ' + name + \
                          '; msg=%s' % e.message) #this isn't showing message
            q.task_done()
            continue #so we don't try to process a query that failed
        #print(str(datetime.datetime.utcnow()) + ' finished executing' + name)

        #process response
        try:
            if err: #note any errors
                logging.error(err)

            #do something with the results
            f = StringIO.StringIO(out)
            reader = csv.DictReader(f)
            for row in reader:
                #skip TLP:RED
                if row['document_tlp'].lower() == 'red':
                    continue
                #skip NOFORN
                if row['document_noforn'].lower() == 'true':
                    continue
                #skip NOCOMM
                if row['document_nocomm'].lower() == 'true':
                    continue

                #skip non-routable IPs, networks
                if row['observable_variety'] == 'ip': #ip
                    ip = netaddr.IPAddress(row['observable_value'])
                    if (ip.is_private() or ip.is_reserved() or \
                        ip.is_loopback() or ip.is_multicast()):
                            continue
                else: #network
                    net = netaddr.IPNetwork(row['observable_value'])
                    if (net.is_private() or net.is_reserved() or \
                        net.is_loopback() or net.is_multicast()):
                            continue

                #this should really .decode('ascii', 'ignore') each
                #field to a safe string a the outset

                #clean response
                for field in row:
                    if row[field] in ['null', '"null"']:
                        row[field] = ''

                row['ttp_actions'] = row['ttp_actions'].replace('"[','[')
                row['ttp_actions'] = row['ttp_actions'].replace(']"',']')
                row['ttp_actions'] = row['ttp_actions'].replace('\\"','"')
                row['ttp_actions'] = row['ttp_actions'].replace('\\','')
                row['ttp_actions'] = row['ttp_actions'].replace('""','"')

                fields = []

                #MSS idiosyncrasy
                fields.append('IP')

                #IP/network
                if '/' in row['observable_value']:
                    fields.append(row['observable_value'])
                else:
                    fields.append(row['observable_value'] + '/32')
                
                #category
                if row['ttp_category'] is None:
                    fields.append('malicious_host')
                else:
                    fields.append(row['ttp_category'].replace(' ',
                        '_').replace('distributer', 'distributor'))

                #confidence
                fields.append(inputs[name])

                #MSS idiosyncrasy
                fields.append('')
                fields.append('')

                #MSS date
                fields.append(row['valid'][:10].replace('-', '/'))

                #MSS's description field
                if name == 'abuse':
                    if 'palevo' in row['document_name']:
                        fields.append('Palevo IP')
                        output = 'abuse.ch-palevotracker'
                    elif 'spyeye' in row['document_name']:
                        fields.append('SpyEye IP')
                        output = 'abuse.ch-spyeyetracker'
                        #print('adding ' + row['observable_value'])
                    elif 'zeus' in row['document_name']:
                        fields.append('Zeus IP')
                        output = 'abuse.ch-zeustracker'
                    #Feodo lists are left; deal with them
                    elif 'badips' in row['document_name']:
                        fields.append('Feodo Version B')
                        output = 'abuse.ch-feodotracker-b'
                        feodob.append(row['observable_value'])
                    else:
                        continue #other Feodo?
                elif name == 'abuse2':
                    if row['document_name'] == 'https://feodotracker.abuse.ch/blocklist.php?download=ipblocklist':
                        if row['observable_value'] not in feodob:
                            fields.append('Feodo Version A')
                            output = 'abuse.ch-feodotracker-a'
                            #print('Feodo A adding ' + row['observable_value'])
                        else:
                            continue #other Feodo?
                    else:
                        continue #other Feodo?

                elif name == 'atims':
                    output = 'ATIMS'
                    fields.append('; '.join([str(row['document_name']),
                                             'Description=' + \
                                             str(row['description']),
                                             'Actions=' + \
                                             str(row['ttp_actions'])]))
                elif name == 'ciscp':
                    output = 'CISCP'
                    fields.append('; '.join([str(row['description']).replace('|', '-').replace('AttackPhase=C2', 'AttackPhase= C2').replace('Product=IB','Product= IB').replace('AttackPhase=INSTALL', 'AttackPhase= INSTALL'),
                        'Actions=' + \
                        str(row['ttp_actions']).replace('Hacking: Backdoor or C2', 'hacking: backdoor or c2')]))

                elif name == 'csint':
                    output = 'CSINT'
                    if row['document_name']:
                        reference = row['document_name']
                    else:
                        reference = ''
                    if row['description']:
                        description = row['description']
                    else:
                        description = ''
                    fields.append('; '.join(['Reference=' + \
                        reference.decode('ascii', 'ignore'),
                        'Description=' + description.decode('ascii',
                        'ignore'), 'Actions=' + str(row['ttp_actions'])]))

                elif name == 'govint':
                    output = 'GOVINT'
                    if row['description']:
                        description = row['description']
                    else:
                        description = ''
                    fields.append('; '.join(['Document=' + \
                        str(row['document_name']).decode('ascii','ignore'),
                                             'Description=' + \
                        description.decode('ascii','ignore'),
                        'Actions=' + str(row['ttp_actions'])]))

                elif name == 'mss':
                    output = 'MSS'
                    if row['description']:
                        description = row['description']
                    else:
                        description = ''
                    description = description.decode('ascii', 'ignore')
                    fields.append('; '.join([description.replace('|', '-'),
                        'Actions=' + str(row['ttp_actions'])]))
                elif name == 'osint':
                    output = 'OSINT'
                    if row['document_name']:
                        reference = row['document_name']
                    else:
                        reference = ''
                    if row['ttp_actions']:
                        description = row['ttp_actions']
                    else:
                        description = ''
                    fields.append('; '.join(['Reference=' + \
                        reference.decode('ascii', 'ignore'),
                        'Description=' + description.decode('ascii',
                        'ignore'), 'Actions=' + str(row['ttp_actions'])]))
                elif name == 'osint30':
                    output = 'OSINT30'
                    if row['document_name']:
                        reference = row['document_name']
                    else:
                        reference = ''
                    if row['ttp_actions']:
                        description = row['ttp_actions']
                    else:
                        description = ''
                    fields.append('; '.join(['Reference=' + \
                        reference.decode('ascii', 'ignore'),
                        'Description=' + description.decode('ascii',
                        'ignore'), 'Actions=' + str(row['ttp_actions'])]))
                elif name == 'tor':
                    fields.append(row['description'])
                    if 'Tor exit node' in row['description']:
                        output = 'TOR-exit'
                    else:
                        output = 'TOR-entry'
                elif name == 'vecirt':
                    output = 'VECIRT'
                    if row['description']:
                        description = row['description']
                    else:
                        description = ''
                    fields.append('; '.join(['Alert=' + \
                        str(row['document_name']), 'Description=' + \
                        description.decode('ascii','ignore'),
                        'Actions=' + str(row['ttp_actions'])]))
                elif name == 'vzir':
                    output = 'VZIR'
                    if row['description']:
                        description = row['description']
                    else:
                        description = ''
                    fields.append('; '.join(['Case=' + row['document_name'],
                        'Description=' + description.decode('ascii','ignore'),
                        'Actions=' + str(row['ttp_actions'])]))

                if '/32' in fields[1]:
                    record = '|'.join(fields)
                    record = record.replace('\n','-').replace('\r','-')
                    outputs[output]['results'].append(record)
                else: #expand range
                    net = netaddr.IPNetwork(fields[1])
                    for ip in net.iter_hosts():
                        fields[1] = str(ip) + '/32'
                        record = '|'.join(fields).replace('\n',
                            '-').replace('\r','-')
                        outputs[output]['results'].append(record)
            
        except StandardError as e:
            logging.error('Error processing query: ' + name + \
                          '; msg=%s' % e.message)
        print(str(datetime.datetime.utcnow()) + ' finished ' + name)
        sys.stdout.flush()
        q.task_done()

#start thread(s)
for i in range(max_iocdb_queries):
    t = threading.Thread(target=query_iocdb, args = (q,))
    t.daemon = True
    t.start()

#queue IOCDB query(ies) followed by name
arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'abuse.ch',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--received_starting', hour_ago,
             '--encoder', 'csv',
             '--csv_columns', 'abuse']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'CISCP Consolidated Indicators',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--valid_starting', year_ago,
             '--encoder', 'csv',
             '--csv_columns', 'ciscp']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'csint',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--valid_starting', year_ago,
             '--encoder', 'csv',
             '--csv_columns', 'csint']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'govint',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--valid_starting', year_ago,
             '--encoder', 'csv',
             '--csv_columns', 'govint']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'osint',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--valid_starting', year_ago,
             '--encoder', 'csv',
             '--csv_columns', 'osint']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'atims',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--valid_starting', year_ago,
             '--encoder', 'csv',
             '--csv_columns', 'atims']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'mss',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--valid_starting', year_ago,
             '--encoder', 'csv',
             '--csv_columns', 'mss']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'vecirt',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--valid_starting', year_ago,
             '--encoder', 'csv',
             '--csv_columns', 'vecirt']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'vzir',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--valid_starting', year_ago,
             '--encoder', 'csv',
             '--csv_columns', 'vzir']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'Tor Directory Consensus',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--received_starting', hour_ago,
             '--encoder', 'csv',
             '--csv_columns', 'tor']
q.put(arguments)

arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'osint',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--valid_starting', thirty_days_ago,
             '--encoder', 'csv',
             '--csv_columns', 'osint30']
q.put(arguments)

#run this a second time as abuse2 to pick up Feodo A
arguments = ['/usr/local/bin/iocdb', 'rumors',
             '--observable_varieties', 'ip', 'network',
             '--document_sources', 'abuse.ch',
             '--observable_whitelist', 'whitelist_mss_master.txt',
             '--received_starting', hour_ago,
             '--encoder', 'csv',
             '--csv_columns', 'abuse2']
q.put(arguments)

q.join() #block until all queries are done

#add feodo-a and feodo-b to feodo
for result in outputs['abuse.ch-feodotracker-a']['results']:
    result = result.replace('Feodo Version A','Feodo')
    outputs['abuse.ch-feodotracker']['results'].append(result)
for result in outputs['abuse.ch-feodotracker-b']['results']:
    result = result.replace('Feodo Version B','Feodo')
    outputs['abuse.ch-feodotracker']['results'].append(result)
    
try:
    for o in outputs:
        outputs[o]['file'].write('\n'.join(set(outputs[o]['results'])))
        outputs[o]['file'].close()
except StandardError as e:
    logging.error('Error opening files' + '; msg=%s' % e.message)

#reporoduce Support Intelligence from daily CSV, cleaned with whitelist
print(str(datetime.datetime.utcnow()) + ' generating Support Intelligence\n')
sys.stdout.flush()

wl = []
with open('whitelist_mss_master.txt', 'r') as wlf:
    for line in wlf:
        wl.append(line.strip())
            
#copy SI lists checking whitelist
with open('SI-Bot.csv', 'r') as botin:
    with open('/var/data/output/mss/SI-Bot_' + time_stamp + '.dat',
              'w') as botout:
        reader = csv.DictReader(botin)
        for row in reader:
            if row['observable_value'] not in wl:
                if '/' not in row['observable_value']:
                    row['observable_value'] = row['observable_value'] + '/32'
                botout.write('|'.join(['IP',
                                        row['observable_value'],
                                        'bot',
                                        '85',
                                        '',
                                        '',
                                        row['valid'][:10].replace('-', '/'),
                                        row['description'] + '; Actions: ' + \
                                        str(row['ttp_actions'])]) + '\n')

with open('SI-C2.csv', 'r') as botin:
    with open('/var/data/output/mss/SI-C2_' + time_stamp + '.dat',
              'w') as botout:
        reader = csv.DictReader(botin)
        for row in reader:
            if row['observable_value'] not in wl:
                if '/' not in row['observable_value']:
                    row['observable_value'] = row['observable_value'] + '/32'
                botout.write('|'.join(['IP',
                                        row['observable_value'],
                                        'bot',
                                        '85',
                                        '',
                                        '',
                                        row['valid'][:10].replace('-', '/'),
                                        row['description'] + '; Actions: ' + \
                                        str(row['ttp_actions'])]) + '\n')

print(str(datetime.datetime.utcnow()) + ' Deleteing old files from remote.')
os.system("ssh sbrannon@10.114.75.171 'rm /home/mss/files/*'")
print(str(datetime.datetime.utcnow()) + ' Transferring files to remote.')
os.system('scp -C /var/data/output/mss/* sbrannon@10.114.75.171:/home/mss/files/')
print(str(datetime.datetime.utcnow()) + ' Done.')
