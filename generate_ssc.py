#!/usr/bin/env python
import datetime
from datetime import timedelta
import subprocess
import threading
import Queue
import logging
import StringIO
import csv
import sys
import os

stamp = str(datetime.datetime.now())[:19]

columns = ['observable_value', 'ttp_malware', 'campaign_name', 'valid',
           'description', 'ttp_category', 'document_source',
           'observable_variety', 'document_name', 'actor_name',
           'document_received', 'document_noforn', 'document_tlp',
           'document_nocomm', 'ttp_actions']

max_iocdb_queries = 3 #number of IOCDB queries to run at the same time

q = Queue.Queue()

sources = ['atims', 'ids2', 'mss', 'vecirt', 'vzir']

records = []

#query IOCDB for data then process; called for each IOCDB query in parallel
def query_iocdb(q):
    while True: # stay in an infinite loop processing any items in the q
        args = q.get() #thread will block on this until something is in q

        print(str(datetime.datetime.utcnow()) + ' executing ' + \
              str(args[3]) + '\n')
        sys.stdout.flush()

        try_number = 0
        
        try:
            sys.stdout.flush()
            p = subprocess.Popen(args + columns,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            out, err = p.communicate()
        except elasticsearch.exceptions.ConnectionError as e:
            logging.error('Error running query: ' + args[3] + \
                          ' Caught elasticsearch.exceptions.ConnectionError' +\
                          'msg=%s' % e.message)
            try_number += 1
            #try again if this is only the second time
            if try_number <= 1:
                logging.error('Trying again.')
                sys.stdout.flush()
                p = subprocess.Popen(args + columns,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                out, err = p.communicate()
            else:
                logging.error('Already tried twice before this; stopping.')
                q.task_done()
                continue #so we don't try to process a query that failed

        except StandardError as e:
            logging.error('Error running query: ' + args[3] + \
                          '; msg=%s' % e.message)
            q.task_done()
            continue #so we don't try to process a query that failed

        #process response
        print(str(datetime.datetime.utcnow()) + ' processing ' + \
              str(args[3]) + '\n')
        try:
            if err: #note any errors
                logging.error(err)

            #do something with the results

            f = StringIO.StringIO(out)
            reader = csv.DictReader(f)

            for row in reader:
                #skip TLP:RED
                if row['document_tlp'].lower() == 'red':
                    continue
                #skip NOCOMM
                if row['document_nocomm'].lower() == 'true':
                    continue

                csvfile = StringIO.StringIO()
                csvwriter = csv.writer(csvfile)

                #clean response
                for field in row:
                    if row[field] in ['null', '"null"']:
                        row[field] = ''

                row['ttp_actions'] = row['ttp_actions'].replace('"[','[')
                row['ttp_actions'] = row['ttp_actions'].replace(']"',']')
                row['ttp_actions'] = row['ttp_actions'].replace('\\"','"')
                row['ttp_actions'] = row['ttp_actions'].replace('\\','')
                row['ttp_actions'] = row['ttp_actions'].replace('""','"')

                fields = []
                for field in columns:
                    fields.append(row[field])
                csvwriter.writerow(fields)
                records.append(csvfile.getvalue())

        except StandardError as e:
            logging.error('Error processing query: ' + str(args[3]) + \
                          '; msg=%s' % e.message)
        print(str(datetime.datetime.utcnow()) + ' finished ' + args[3])
        sys.stdout.flush()
        q.task_done()

#start thread(s)
for i in range(max_iocdb_queries):
    t = threading.Thread(target=query_iocdb, args = (q,))
    t.daemon = True
    t.start()

#queue IOCDB query(ies)
for source in sources:
    arguments = ['/usr/local/bin/iocdb', 'rumors',
                 '--document_sources', source,
                 '--encoder', 'csv',
                 '--csv_columns']
    q.put(arguments)

q.join() #block until all queries are done

#delete old files
folder = '/var/data/output/ssc'
for the_file in os.listdir(folder):
    file_path = os.path.join(folder, the_file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
    except Exception, e:
        print e

try:
    filename = str(datetime.datetime.now())[:10] + '_ssc'
    with open('/var/data/output/ssc/' + filename + '.csv', 'w') as f:
        f.write(','.join(columns) + '\n')        
        f.write(''.join(set(records)))

except StandardError as e:
    logging.error('Error opening files' + '; msg=%s' % e.message)

print(str(datetime.datetime.utcnow()) + ' Deleteing old files from remote.')
os.system("ssh sbrannon@10.114.75.168 'rm /home/ssc/files/*'")
print(str(datetime.datetime.utcnow()) + ' Transferring files to remote.')
os.system('scp -C /var/data/output/ssc/* sbrannon@10.114.75.168:/home/ssc/files/')
print(str(datetime.datetime.utcnow()) + ' Done.')
