#!/usr/bin/env python
import datetime
from datetime import timedelta
import subprocess
import threading
import Queue
import logging
import StringIO
import csv
import sys
import os

day_ago = str(datetime.datetime.now() - timedelta(days=1))[:19]
day_ago = day_ago.replace(' ', 'T')
thirty_days_ago = str(datetime.datetime.now() - timedelta(days=30))[:19]
thirty_days_ago = thirty_days_ago.replace(' ', 'T')

#keys are output names; values are sources
feeds = {'alienvault': {'source' : 'AlienVault Reputation Data Feed'}, 
        'ATIMS': {'source' : 'atims'}, 
        'CISCP': {'source' : 'CISCP Consolidated Indicators'}, 
        'CSINT': {'source' : 'csint'}, 
        'GOVINT': {'source' : 'govint'},
        'IDS2': {'source' : 'ids2'}, 
        'OSINT': {'source' : 'osint'},
        'VECIRT': {'source' : 'vecirt'}, 
        'VZIR': {'source' : 'vzir'}}

columns = ['observable_value', 'valid', 'document_tlp', 'ttp_category',
           'ttp_actions', 'description', 'document_nocomm']

max_iocdb_queries = 3 #number of IOCDB queries to run at the same time

q = Queue.Queue()

#delete old files
folder = '/var/data/output/atd'
for the_file in os.listdir(folder):
    file_path = os.path.join(folder, the_file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
    except Exception, e:
        print e

#open files for output
try:
    for feed in feeds.keys():
        feeds[feed]['file'] = open('/var/data/output/atd/' + \
                                       feed + '_IP.csv', 'w')
except StandardError as e:
    logging.error('Error opening files' + '; msg=%s' % e.message)

#create holders for results
for feed in feeds:
    feeds[feed]['results'] = []

#query IOCDB for data then process; called for each IOCDB query in parallel
def query_iocdb(q):
    while True: # stay in an infinite loop processing any items in the q
        args = q.get() #thread will block on this until something is in q
        feed = args.pop()
        print(str(datetime.datetime.utcnow()) + ' executing ' + feed + '\n')
        sys.stdout.flush()

        try_number = 0
        
        try:
            sys.stdout.flush()
            p = subprocess.Popen(args + columns,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            out, err = p.communicate()
        except elasticsearch.exceptions.ConnectionError as e:
            logging.error('Error running query: ' + args[5] + ' ' + args[3] + \
                          ' Caught elasticsearch.exceptions.ConnectionError' +\
                          'msg=%s' % e.message)
            try_number += 1
            #try again if this is only the second time
            if try_number <= 1:
                logging.error('Trying again.')
                sys.stdout.flush()
                p = subprocess.Popen(args + columns,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                out, err = p.communicate()
            else:
                logging.error('Already tried twice before this; stopping.')
                q.task_done()
                continue #so we don't try to process a query that failed

        except StandardError as e:
            logging.error('Error running query: ' + feed + \
                          '; msg=%s' % e.message)
            q.task_done()
            continue #so we don't try to process a query that failed

        #process response
        print(str(datetime.datetime.utcnow()) + ' processing ' + feed + '\n')
        try:
            if err: #note any errors
                logging.error(err)

            #do something with the results

            f = StringIO.StringIO(out)
            reader = csv.DictReader(f)

            for row in reader:
                #skip TLP:RED
                if row['document_tlp'].lower() == 'red':
                    continue
                #skip NOCOMM
                if row['document_nocomm'].lower() == 'true':
                    continue

                csvfile = StringIO.StringIO()
                csvwriter = csv.writer(csvfile)

                #clean response
                for field in row:
                    if row[field] in ['null', '"null"']:
                        row[field] = ''

                row['ttp_actions'] = row['ttp_actions'].replace('"[','[')
                row['ttp_actions'] = row['ttp_actions'].replace(']"',']')
                row['ttp_actions'] = row['ttp_actions'].replace('\\"','"')
                row['ttp_actions'] = row['ttp_actions'].replace('\\','')
                row['ttp_actions'] = row['ttp_actions'].replace('""','"')

                fields = []
                for field in columns[:-1]: #leave off document_nocomm
                    if field == 'valid':
                        fields.append(row[field].replace('T', ' '))
                    else:
                        fields.append(row[field])
                csvwriter.writerow(fields)
                feeds[feed]['results'].append(csvfile.getvalue())

        except StandardError as e:
            logging.error('Error processing query: ' + feed + \
                          '; msg=%s' % e.message)
        print(str(datetime.datetime.utcnow()) + ' finished ' + feed + '\n')
        sys.stdout.flush()
        q.task_done()

#start thread(s)
for i in range(max_iocdb_queries):
    t = threading.Thread(target=query_iocdb, args = (q,))
    t.daemon = True
    t.start()

#queue IOCDB query(ies) followed by feed name
for feed in feeds:
    if feed == 'alienvault':
        arguments = ['/usr/local/bin/iocdb', 'rumors',
                     '--received_starting', day_ago,
                     '--document_sources', feeds[feed]['source'],
                     '--observable_whitelist', 'atd_master_whitelist.txt',
                     '--encoder', 'csv',
                     '--csv_columns', feed]
    else:
        arguments = ['/usr/local/bin/iocdb', 'rumors',
                     '--observable_varieties', 'ip',
                     '--valid_starting', thirty_days_ago,
                     '--document_sources', feeds[feed]['source'],
                     '--observable_whitelist', 'atd_master_whitelist.txt',
                     '--encoder', 'csv',
                     '--csv_columns', feed]        
    q.put(arguments)

q.join() #block until all queries are done

try:
    for feed in feeds:
        feeds[feed]['file'].write(''.join(set(feeds[feed]['results'])))
        feeds[feed]['file'].close()        
except StandardError as e:
    logging.error('Error writing files' + '; msg=%s' % e.message)

print(str(datetime.datetime.utcnow()) + ' Deleteing old files from remote.')
os.system("ssh sbrannon@10.114.75.167 'rm /home/ni/files/atd/*'")
print(str(datetime.datetime.utcnow()) + ' Transferring files to remote.')
os.system('scp -C /var/data/output/atd/* sbrannon@10.114.75.167:/home/ni/files/atd/')
print(str(datetime.datetime.utcnow()) + ' Done.')
