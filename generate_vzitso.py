#!/usr/bin/env python
import datetime
from datetime import timedelta
import subprocess
import threading
import Queue
import logging
import StringIO
import csv
import sys
import os
import gzip

hour_ago = str(datetime.datetime.now() - timedelta(hours=1))[:14]
hour_ago = hour_ago.replace(' ', 'T') + '00:00'

end_date = str(datetime.datetime.now())[:14]
end_date = end_date.replace(' ', 'T') + '00:00'

file_base = hour_ago + '-' + end_date[11:]

columns = ['observable_value', 'ttp_malware', 'campaign_name',
           'valid', 'ttp_category', 'document_source', 'observable_variety',
           'document_name', 'actor_name', 'document_received',
           'document_noforn', 'document_tlp', 'document_nocomm',
           'ttp_actions', 'description']

max_iocdb_queries = 3 #number of IOCDB queries to run at the same time

q = Queue.Queue()

sources = {'abuse.ch',
           'AlienVault Reputation Data Feed',
           'atims',
           'Autoshun.org Shunlist',
           'Bambenek Consulting',
           'CIArmy IP Blocklist',
           'CISCP Consolidated Indicators',
           'Comodo SiteInspector Recent Detections',
           'csint',
           'govint',
           'ids2',
           'Malc0de',
           'MalwareDB Threats',
           'MalwareDomainList Incremental Update',
           'mss',
           'NoThink Blocklists',
           'OpenBL IP List',
           'osint',
           'Project Honeypot',
           'Rulez.sk BruteForceBlocker',
           'security-research.dyndns.org',
           'SANS ISC Attack Sources',
           'Shadowserver',
           'Support-Intelligence Feed',
           'Support-Intelligence URL Feed',
           'Tor Directory Consensus',
           'vecirt',
           'VIRBL IP List',
           'VirusTotal URL Feed',
           'VxVault Malware Data',
           'vzir'
           }

#delete old files
folder = '/var/data/output/vzitso'
for the_file in os.listdir(folder):
    file_path = os.path.join(folder, the_file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
    except Exception, e:
        print e

records = []

#query IOCDB for data then process; called for each IOCDB query in parallel
def query_iocdb(q):
    while True: # stay in an infinite loop processing any items in the q
        args = q.get() #thread will block on this until something is in q
        name = args.pop()
        
        print(str(datetime.datetime.utcnow()) + ' executing ' + name + '\n')
        sys.stdout.flush()

        try_number = 0
        
        try:
            sys.stdout.flush()
            p = subprocess.Popen(args + columns,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            out, err = p.communicate()

        except elasticsearch.exceptions.ConnectionError as e:
            logging.error('Error running query: ' + args[5] + ' ' + name + \
                          ' Caught elasticsearch.exceptions.ConnectionError' +\
                          'msg=%s' % e.message)
            try_number += 1
            #try again if this is only the second time
            if try_number <= 1:
                logging.error('Trying again.')
                sys.stdout.flush()
                p = subprocess.Popen(args + columns,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                out, err = p.communicate()
            else:
                logging.error('Already tried twice before this; stopping.')
                q.task_done()
                continue #so we don't try to process a query that failed

        except StandardError as e:
            logging.error('Error running query: ' + name + \
                          '; msg=%s' % e.message)
            q.task_done()
            continue #so we don't try to process a query that failed

        #process response
        print(str(datetime.datetime.utcnow()) + ' processing ' + name + '\n')
        try:
            if err: #note any errors
                logging.error(err)

            #do something with the results

            f = StringIO.StringIO(out)
            reader = csv.DictReader(f)

            for row in reader:
                #skip TLP:RED
                if row['document_tlp'].lower() == 'red':
                    continue
                #skip NOFORN
                if row['document_noforn'].lower() == 'true':
                    continue
                
                csvfile = StringIO.StringIO()
                csvwriter = csv.writer(csvfile)

                #clean response
                for field in row:
                    if row[field] in ['null', '"null"']:
                        row[field] = ''

                row['ttp_actions'] = row['ttp_actions'].replace('"[','[')
                row['ttp_actions'] = row['ttp_actions'].replace(']"',']')
                row['ttp_actions'] = row['ttp_actions'].replace('\\"','"')
                row['ttp_actions'] = row['ttp_actions'].replace('\\','')
                row['ttp_actions'] = row['ttp_actions'].replace('""','"')

                fields = []
                for field in columns:
                    if field == 'description':
                        fields.append(row[field].replace(',', '-'))
                    elif field == 'ttp_actions':
                        #csv writer turns quote into three quotes
                        #might as well clean since they're using regexes
                        fields.append(row[field].replace('"', ''))
                    else:
                        fields.append(row[field])
                csvwriter.writerow(fields)
                records.append(csvfile.getvalue())

        except StandardError as e:
            logging.error('Error processing query: ' + name + \
                          '; msg=%s' % e.message)
        print(str(datetime.datetime.utcnow()) + ' finished ' + name + '\n')
        sys.stdout.flush()
        q.task_done()

#start thread(s)
for i in range(max_iocdb_queries):
    t = threading.Thread(target=query_iocdb, args = (q,))
    t.daemon = True
    t.start()

#queue IOCDB query(ies) followed by name
for source in sources:
    arguments = ['/usr/local/bin/iocdb', 'rumors',
                 '--received_starting', hour_ago,
                 '--received_ending', end_date,
                 '--document_sources', source,
                 '--encoder', 'csv',
                 '--csv_columns', source]
    q.put(arguments)

q.join() #block until all queries are done

try:
    with gzip.open('/var/data/output/vzitso/' + file_base + \
              '.csv.gz', 'wb') as f:
        f.write(','.join(columns) + '\n')
        f.write(''.join(set(records)))
        
except StandardError as e:
    logging.error('Error opening files' + '; msg=%s' % e.message)

print(str(datetime.datetime.utcnow()) + ' Deleteing old files from remote.')
os.system("ssh sbrannon@10.114.75.168 'find /home/vzitso/files/* -mtime +3 -exec rm -f '{}' \;'")
print(str(datetime.datetime.utcnow()) + ' Transferring files to remote.')
os.system('scp -C /var/data/output/vzitso/* sbrannon@10.114.75.168:/home/vzitso/files/')
print(str(datetime.datetime.utcnow()) + ' Done.')
