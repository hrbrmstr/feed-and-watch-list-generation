#!/usr/bin/env python
import datetime
from datetime import timedelta
import subprocess
import threading
import Queue
import logging
import StringIO
import csv
import sys

day_ago = str(datetime.datetime.now() - timedelta(days=1))[:19]
day_ago = day_ago.replace(' ', 'T')

columns = ['observable_value', 'document_source', 'document_name',
           'valid', 'document_tlp', 'ttp_category', 'ttp_actions',
           'description', 'document_nocomm', 'document_noforn']

max_iocdb_queries = 3 #number of IOCDB queries to run at the same time

q = Queue.Queue()

varieties = ['ip', 'network', 'domain', 'md5', 'sha1', 'sha256']

sources = ['AlienVault Reputation Data Feed', 'atims',
           'CISCP Consolidated Indicators', 'csint', 'govint', 'ids2',
          'osint', 'vecirt', 'vzir']

outputs = {'ip' : {},
           'domain' : {},
           'hash' : {},
           }

#open files for output and add data holders to dictionaries
try:
    for output in outputs:
        outputs[output]['txt'] = open('/var/data/output/ir/' + \
                                       output + '.txt', 'w')
        outputs[output]['csv'] = open('/var/data/output/ir/' + \
                                       output + '.csv', 'w')
        outputs[output]['observables'] = []
        outputs[output]['records'] = []
except StandardError as e:
    logging.error('Error opening files' + '; msg=%s' % e.message)

#query IOCDB for data then process; called for each IOCDB query in parallel
def query_iocdb(q):
    while True: # stay in an infinite loop processing any items in the q
        args = q.get() #thread will block on this until something is in q
        variety = args.pop()
        sys.stdout.flush()
        print(str(datetime.datetime.utcnow()) + ' executing ' + \
              str(args[5]) + ' ' + str(args[3])+ '\n')
        sys.stdout.flush()

        try_number = 0
        
        try:
            sys.stdout.flush()
            p = subprocess.Popen(args + columns,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            out, err = p.communicate()
        except elasticsearch.exceptions.ConnectionError as e:
            logging.error('Error running query: ' + args[5] + ' ' + args[3] + \
                          ' Caught elasticsearch.exceptions.ConnectionError' +\
                          'msg=%s' % e.message)
            try_number += 1
            #try again if this is only the second time
            if try_number <= 1:
                logging.error('Trying again.')
                sys.stdout.flush()
                p = subprocess.Popen(args + columns,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                out, err = p.communicate()
            else:
                logging.error('Already tried twice before this; stopping.')
                q.task_done()
                continue #so we don't try to process a query that failed

        except StandardError as e:
            logging.error('Error running query: ' + args[5] + ' ' + \
                          args[3] + '; msg=%s' % e.message)
            q.task_done()
            continue #so we don't try to process a query that failed

        #process response
        print(str(datetime.datetime.utcnow()) + ' processing ' + \
              str(args[5]) + ' ' + str(args[3])+ '\n')
        try:
            if err: #note any errors
                logging.error(err)

            #do something with the results
            #map real variety to output
            if variety == 'network':
                variety = 'ip'
            elif variety in ['md5', 'sha1', 'sha256']:
                variety = 'hash'
            f = StringIO.StringIO(out)
            reader = csv.DictReader(f)

            for row in reader:
                #skip TLP:RED
                if row['document_tlp'].lower() == 'red':
                    continue
                #skip NOFORN
                if row['document_noforn'].lower() == 'true':
                    continue
                #skip NOCOMM
                if row['document_nocomm'].lower() == 'true':
                    continue

                #clean response
                for field in row:
                    if row[field] in ['null', '"null"']:
                        row[field] = ''

                row['ttp_actions'] = row['ttp_actions'].replace('"[','[')
                row['ttp_actions'] = row['ttp_actions'].replace(']"',']')
                row['ttp_actions'] = row['ttp_actions'].replace('\\"','"')
                row['ttp_actions'] = row['ttp_actions'].replace('\\','')
                row['ttp_actions'] = row['ttp_actions'].replace('""','"')
                
                outputs[variety]['observables'].append(row['observable_value'])

                csvfile = StringIO.StringIO()
                csvwriter = csv.writer(csvfile)
                csvwriter.writerow([row['observable_value'],
                                    row['document_source'],
                                    row['document_name'],
                                    row['valid'],
                                    row['document_tlp'],
                                    row['ttp_category'],
                                    row['ttp_actions'],
                                    row['description']])
                outputs[variety]['records'].append(csvfile.getvalue())

        except StandardError as e:
            logging.error('Error processing query: ' + str(args[5]) + ' ' + \
                          str(args[3]) + '; msg=%s' % e.message)
        print(str(datetime.datetime.utcnow()) + ' finished ' + args[5] + \
              ' ' + args[3])
        sys.stdout.flush()
        q.task_done()

#start thread(s)
for i in range(max_iocdb_queries):
    t = threading.Thread(target=query_iocdb, args = (q,))
    t.daemon = True
    t.start()

#queue IOCDB query(ies) followed by variety
for source in sources:
    for variety in varieties:
        if source != 'AlienVault Reputation Data Feed': #normal
            arguments = ['/usr/local/bin/iocdb', 'rumors',                     
                         '--observable_varieties', variety,
                         '--document_sources', source,
                         '--encoder', 'csv',
                         '--csv_columns', variety]
        else: #AlienVault need date limit    
            arguments = ['/usr/local/bin/iocdb', 'rumors',                     
                         '--observable_varieties', variety,
                         '--document_sources', source,
                         '--valid_starting', day_ago,
                         '--encoder', 'csv',
                         '--csv_columns', variety]
        q.put(arguments)

q.join() #block until all queries are done

try:
    for o in outputs:
        print(str(datetime.datetime.utcnow()) + ' writing ' + str(o))
        outputs[o]['txt'].write('\n'.join(set(outputs[o]['observables'])))
        outputs[o]['csv'].write('indicator,source,name,valid,tlp,category,' + \
                                'actions,description\n')
        outputs[o]['csv'].write(''.join(set(outputs[o]['records'])))
        outputs[o]['txt'].close()
        outputs[o]['csv'].close()
except StandardError as e:
    logging.error('Error opening files' + '; msg=%s' % e.message)
print(str(datetime.datetime.utcnow()) + ' Done.')
