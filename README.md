feed-and-watch-list-generation
==============================

Collection of Python scripts to pull data from IOCDB and produce feed and watch list files for partners

The "IOCDB Partners.xlsx" spreadsheet lists which partners we support on which servers with which sources. The ideas is that if anything goes wrong, anyone can see immediately who's impacted and who to email to communicate with the partner. 

Watch list generation happens on feed-client-63 under the sbrannon account, and then the resulting files are transferred from /var/data/output/ to the old servers where partners are already configured to retrieve them. 

To trace execution, start with the sbrannon account's crontab. There's a scheduled task for each generation python script that writes detailed logs (and errors) to a corresponding log file. For example, generate _ mss.py logs its activity to generate _ mss.log. 

The only complexity in the code is that it queues up each piece of what a partner needs as a separate query in its own thread, and then executes up to 3 of those threads in parallel for speed and independence. Other than that, each script basically just queries IOCDB for the data it needs, and then writes it to a file the way the partner expects it. 
Most of the scripts ultimately put files in the partner's /home/partner/files/ directory, and then the partner retrieves them using SFTP. 