#!/usr/bin/env python
import datetime
import pytz
from datetime import timedelta
import subprocess
import threading
import Queue
import logging
import StringIO
import csv
import sys
import os
import hashlib

#only execute at 0, 8, and 16 hours Eastern
utc = pytz.utc
d = utc.localize(datetime.datetime.now())
h = d.astimezone(pytz.timezone('America/New_York')).hour

if h not in (0, 8, 16):
    exit()

curtime = datetime.datetime.now()

starttime = (str(curtime - timedelta(days=14))[:14] + '00:00').replace(' ','T')
stamp = str(datetime.datetime.now())[:19]

columns = ['document_source', 'observable_variety', 'observable_value', 'valid',
           'document_received', 'document_tlp', 'ttp_category', 'ttp_actions',
           'description', 'document_nocomm']

max_iocdb_queries = 3 #number of IOCDB queries to run at the same time

q = Queue.Queue()

sources = ['abuse.ch', 'atims', 'CISCP Consolidated Indicators', 'csint',
           'govint', 'osint', 'Shadowserver','vecirt', 'vzir']

records = []

#query IOCDB for data then process; called for each IOCDB query in parallel
def query_iocdb(q):
    while True: # stay in an infinite loop processing any items in the q
        args = q.get() #thread will block on this until something is in q

        print(str(datetime.datetime.utcnow()) + ' executing ' + \
              str(args[5]) + ' ' + str(args[3])+ '\n')
        sys.stdout.flush()

        try_number = 0
        
        try:
            sys.stdout.flush()
            p = subprocess.Popen(args + columns,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            out, err = p.communicate()
        except elasticsearch.exceptions.ConnectionError as e:
            logging.error('Error running query: ' + args[5] + ' ' + args[3] + \
                          ' Caught elasticsearch.exceptions.ConnectionError' +\
                          'msg=%s' % e.message)
            try_number += 1
            #try again if this is only the second time
            if try_number <= 1:
                logging.error('Trying again.')
                sys.stdout.flush()
                p = subprocess.Popen(args + columns,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                out, err = p.communicate()
            else:
                logging.error('Already tried twice before this; stopping.')
                q.task_done()
                continue #so we don't try to process a query that failed

        except StandardError as e:
            logging.error('Error running query: ' + args[5] + ' ' + \
                          args[3] + '; msg=%s' % e.message)
            q.task_done()
            continue #so we don't try to process a query that failed

        #process response
        print(str(datetime.datetime.utcnow()) + ' processing ' + \
              str(args[5]) + ' ' + str(args[3])+ '\n')
        try:
            if err: #note any errors
                logging.error(err)

            #do something with the results

            f = StringIO.StringIO(out)
            reader = csv.DictReader(f)

            for row in reader:
                #skip TLP:RED
                if row['document_tlp'].lower() == 'red':
                    continue
                #skip NOCOMM
                if row['document_nocomm'].lower() == 'true':
                    continue
                del row['document_nocomm']

                csvfile = StringIO.StringIO()
                csvwriter = csv.writer(csvfile)

                #clean response
                for field in row:
                    if row[field] in ['null', '"null"']:
                        row[field] = ''

                row['ttp_actions'] = row['ttp_actions'].replace('"[','[')
                row['ttp_actions'] = row['ttp_actions'].replace(']"',']')
                row['ttp_actions'] = row['ttp_actions'].replace('\\"','"')
                row['ttp_actions'] = row['ttp_actions'].replace('\\','')
                row['ttp_actions'] = row['ttp_actions'].replace('""','"')

                fields = []
                for field in columns[:-1]: #don't need 'document_nocomm'
                    if field in ['valid', 'document_received']:
                        fields.append(row[field][:19].replace('T', ' '))
                    else:
                        fields.append(row[field])
                csvwriter.writerow(fields)
                records.append(csvfile.getvalue())

        except StandardError as e:
            logging.error('Error processing query: ' + str(args[5]) + ' ' + \
                          str(args[3]) + '; msg=%s' % e.message)
        print(str(datetime.datetime.utcnow()) + ' finished ' + args[5] + \
              ' ' + args[3])
        sys.stdout.flush()
        q.task_done()

#start thread(s)
for i in range(max_iocdb_queries):
    t = threading.Thread(target=query_iocdb, args = (q,))
    t.daemon = True
    t.start()

#queue IOCDB query(ies)
for source in sources:
    arguments = ['/usr/local/bin/iocdb', 'rumors',
                 '--received_starting', starttime,
                 '--document_sources', source,
                 '--encoder', 'csv',
                 '--csv_columns']
    q.put(arguments)

q.join() #block until all queries are done

#delete old files
folder = '/var/data/output/atims'
for the_file in os.listdir(folder):
    file_path = os.path.join(folder, the_file)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
    except Exception, e:
        print e

try:
    #filename = str(datetime.datetime.now())[:19].replace(' ', '-') + '_quality'
    filename = str(d.astimezone(pytz.timezone('America/New_York')))[:19].replace(' ', '-') + '_quality'
    with open('/var/data/output/atims/' + filename + '.csv', 'w') as f:
        f.write(','.join(columns) + '\n')
        f.write(''.join(set(records)))

except StandardError as e:
    logging.error('Error opening files' + '; msg=%s' % e.message)

#calculate md5 for file
m = hashlib.md5()
try:
    with open('/var/data/output/atims/' + filename + '.csv', 'rb') as f:
        chunk = f.read(8192)
        while chunk:
            m.update(chunk)
            chunk = f.read(8192)

    with open('/var/data/output/atims/' + filename + '.md5', 'w') as f:
        f.write(str(m.hexdigest()))

except StandardError as e:
    logging.error('Error producing hash' + '; msg=%s' % e.message)

print(str(datetime.datetime.utcnow()) + ' Deleteing old files from remote.')
os.system("ssh sbrannon@10.114.75.168 'rm /home/atims/files/*'")
print(str(datetime.datetime.utcnow()) + ' Transferring files to remote.')
os.system('scp -C /var/data/output/atims/* sbrannon@10.114.75.168:/home/atims/files/')

print(str(datetime.datetime.utcnow()) + ' Done.')
