#!/usr/bin/env python
import datetime
from datetime import timedelta
import subprocess
import threading
import Queue
import logging
import StringIO
import csv
import sys

day_ago = str(datetime.datetime.now() - timedelta(days=1))[:19]
day_ago = day_ago.replace(' ', 'T')
stamp = str(datetime.datetime.now())[:10]

columns = ['observable_value', 'observable_variety', 'valid', 'document_source',
           'document_name', 'document_tlp', 'document_nocomm',
           'document_noforn', 'document_investigator', 'ttp_actions',
           'ttp_category', 'ttp_malware', 'description']

max_iocdb_queries = 3 #number of IOCDB queries to run at the same time

q = Queue.Queue()

sources = ['atims', 'csint', 'govint', 'ids2', 'osint', 'vecirt', 'vzir']

records = []

#query IOCDB for data then process; called for each IOCDB query in parallel
def query_iocdb(q):
    while True: # stay in an infinite loop processing any items in the q
        args = q.get() #thread will block on this until something is in q

        print(str(datetime.datetime.utcnow()) + ' executing ' + \
              str(args[5]) + ' ' + str(args[3])+ '\n')
        sys.stdout.flush()

        try_number = 0
        
        try:
            sys.stdout.flush()
            p = subprocess.Popen(args + columns,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            out, err = p.communicate()
        except elasticsearch.exceptions.ConnectionError as e:
            logging.error('Error running query: ' + args[5] + ' ' + args[3] + \
                          ' Caught elasticsearch.exceptions.ConnectionError' +\
                          'msg=%s' % e.message)
            try_number += 1
            #try again if this is only the second time
            if try_number <= 1:
                logging.error('Trying again.')
                sys.stdout.flush()
                p = subprocess.Popen(args + columns,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                out, err = p.communicate()
            else:
                logging.error('Already tried twice before this; stopping.')
                q.task_done()
                continue #so we don't try to process a query that failed

        except StandardError as e:
            logging.error('Error running query: ' + args[5] + ' ' + \
                          args[3] + '; msg=%s' % e.message)
            q.task_done()
            continue #so we don't try to process a query that failed

        #process response
        print(str(datetime.datetime.utcnow()) + ' processing ' + \
              str(args[5]) + ' ' + str(args[3])+ '\n')
        try:
            if err: #note any errors
                logging.error(err)

            #do something with the results

            f = StringIO.StringIO(out)
            reader = csv.DictReader(f)

            for row in reader:
                #skip TLP:RED
                if row['document_tlp'].lower() == 'red':
                    continue

                csvfile = StringIO.StringIO()
                csvwriter = csv.writer(csvfile)

                #clean response
                for field in row:
                    if row[field] in ['null', '"null"']:
                        row[field] = ''

                row['ttp_actions'] = row['ttp_actions'].replace('"[','[')
                row['ttp_actions'] = row['ttp_actions'].replace(']"',']')
                row['ttp_actions'] = row['ttp_actions'].replace('\\"','"')
                row['ttp_actions'] = row['ttp_actions'].replace('\\','')
                row['ttp_actions'] = row['ttp_actions'].replace('""','"')

                fields = []
                for field in columns:
                    fields.append(row[field])
                csvwriter.writerow(fields)
                records.append(csvfile.getvalue())

        except StandardError as e:
            logging.error('Error processing query: ' + str(args[5]) + ' ' + \
                          str(args[3]) + '; msg=%s' % e.message)
        print(str(datetime.datetime.utcnow()) + ' finished ' + args[5] + \
              ' ' + args[3])
        sys.stdout.flush()
        q.task_done()

#start thread(s)
for i in range(max_iocdb_queries):
    t = threading.Thread(target=query_iocdb, args = (q,))
    t.daemon = True
    t.start()

#queue IOCDB query(ies)
for source in sources:
    arguments = ['/usr/local/bin/iocdb', 'rumors',
                 '--received_starting', day_ago,
                 '--document_sources', source,
                 '--encoder', 'csv',
                 '--csv_columns']
    q.put(arguments)

q.join() #block until all queries are done

try:
    with open('/var/data/output/ni/ni_daily_' + stamp + '.csv', 'w') as f:
        f.write('rumors.observable.value,rumors.observable.variety,' + \
                'rumors.valid,rumors.document.source,rumors.document.name,' + \
                'rumors.document.tlp,rumors.document.nocomm,' + \
                'rumors.document.noforn,rumors.document.investigator,' + \
                'rumors.ttp.actions,rumors.ttp.category,rumors.ttp.malware,' + \
                'rumors.description\n')
        f.write(''.join(set(records)))
        
except StandardError as e:
    logging.error('Error opening files' + '; msg=%s' % e.message)
print(str(datetime.datetime.utcnow()) + ' Done.')
